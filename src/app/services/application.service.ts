import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { AuthService } from './auth.service';


@Injectable({
  providedIn: 'root'
})

export class ApplicationService {

  private requestUrl = 'http://localhost:8000/api';

  constructor(private http: HttpClient, private authService: AuthService) { }

  onRemoveFavourite(internal_id){
    const myHeaders = new HttpHeaders().set('authorization', `Bearer ${this.authService.getAuthToken}`);
    return this.http.post<any>(`${this.requestUrl}/removeFavourite`, {internal_id: internal_id},{headers: myHeaders});
  }

  saveFavourite(internal_id, title, link, author_name, creation_date){
    const myHeaders = new HttpHeaders().set('authorization', `Bearer ${this.authService.getAuthToken}`);
    return this.http.post<any>(`${this.requestUrl}/saveFavourite`, {internal_id: internal_id, title: title, link: link, author_name: author_name, creation_date: creation_date}, {headers: myHeaders});
  }

  getFavourite(){
    const myHeaders = new HttpHeaders().set('authorization', `Bearer ${this.authService.getAuthToken}`);
    return this.http.get<any>(`${this.requestUrl}/getFavourite`, {headers: myHeaders});
  }


  search(queryString: string, pageNumber: number, prePage: number): Observable<any>{
    const myHeaders = new HttpHeaders().set('authorization', `Bearer ${this.authService.getAuthToken}`).set('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS').set('Access-Control-Allow-Origin', '*');;
    return this.http.get(`${this.requestUrl}/search?query=${queryString}&page=${pageNumber}&perPage=${prePage}`, {headers: myHeaders});
  }

  getQueryHistory(): Observable<any>{
    return this.http.get(`${this.requestUrl}/getQueryHistory`);
  }
}
