import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Question} from '../models/question';

@Injectable({
  providedIn: 'root'
})
export class ApplicationService {

  private requestUrl = 'http://localhost:8000/api';

  constructor(private http: HttpClient) {
  }

  search(queryString: string): Observable<Question[]> {
    return this.http.get<Question[]>(`${this.requestUrl}/search?query=${queryString}`);
  }
}
