import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private requestUrl = 'http://localhost:8000/api';

  public token = localStorage.getItem('token') ? localStorage.getItem('token') : '';
  public userName = localStorage.getItem('userName') ? localStorage.getItem('userName') : '';

  constructor(private http: HttpClient) {
  }

  public setAuthToken(token: string) {
    this.token = token;
    localStorage.setItem('token', token);
  }

  public setUserName(userName: string) {
    this.userName = userName;
    localStorage.setItem('userName', userName);
  }

  get getAuthToken() {
    return this.token;
  }


  public login(login: string, password: string): Observable<any> {
    const myHeaders = new HttpHeaders().set('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS').set('Access-Control-Allow-Origin', '*');
    return this.http.post<any>(`${this.requestUrl}/login`, {login: login, password: password}, {headers: myHeaders});
  }

  public register(name, login, email, password, r_password) {
    return this.http.post<any>(`${this.requestUrl}/register`, {
      name: name,
      login: login,
      email,
      password: password,
      r_password: r_password
    });
  }

  public logout() {
    localStorage.clear();
    return this.http.post<any>(`${this.requestUrl}/logout`, {});
  }

}
