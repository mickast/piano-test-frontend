import {Component, OnInit} from '@angular/core';
import {ApplicationService} from '../services/application.service';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  public questions: Array<any> = JSON.parse(localStorage.getItem('questions')) ? JSON.parse(localStorage.getItem('questions')) : [];

  public page: number = localStorage.getItem('page') ? +localStorage.getItem('page') : 1;

  public prePage: number = 10;

  public questionCount: number = localStorage.getItem('questionCount') ? +localStorage.getItem('questionCount') : 0;

  public pageArray: Array<any> = JSON.parse(localStorage.getItem('pageArray')) ? JSON.parse(localStorage.getItem('pageArray')) : [];

  public pageCount: number = localStorage.getItem('pageCount') ? +localStorage.getItem('pageCount') : 0;

  public visibilityStyle: string = 'hidden';

  public searchQuery: string = localStorage.getItem('searchQuery') ? localStorage.getItem('searchQuery') : '';

  public pCountFrom: number = localStorage.getItem('pCountFrom') ? +localStorage.getItem('pCountFrom') : 0;

  public pCountTo: number = localStorage.getItem('pCountTo') ? +localStorage.getItem('pCountTo') : 0;

  constructor(private applicationService: ApplicationService, private authService: AuthService, private router: Router) {

  }

  ngOnInit() {
    if (this.authService.getAuthToken == '') {
      this.router.navigate(['/login']);
    }
  }

  onSearch(searchQuery: string, pageNumber: number): void {

    if (pageNumber == 0 || (pageNumber > this.pageCount && this.pageCount > 0)) return;

    localStorage.setItem('searchQuery', searchQuery);
    this.visibilityStyle = 'visible';

    this.page = pageNumber;
    localStorage.setItem('page', String(this.page));
    this.applicationService.search(searchQuery, pageNumber ? pageNumber : this.page, this.prePage).subscribe(data => {

      this.questions = data.resData.questions;
      localStorage.setItem('questions', JSON.stringify(this.questions));

      this.questionCount = +data.resData.total;
      localStorage.setItem('questionCount', this.questionCount.toString());

      this.pageCount = Math.ceil(+this.questionCount / +this.prePage);
      localStorage.setItem('pageCount', this.pageCount.toString());

      this.pageArray = this.getPaginationArray(this.questionCount, this.prePage, this.page);
      localStorage.setItem('pageArray', JSON.stringify(this.pageArray));

      this.pCountFrom = this.prePage * this.page - this.prePage + 1;
      localStorage.setItem('pCountFrom', this.pCountFrom.toString());

      this.pCountTo = this.prePage * this.page;
      localStorage.setItem('pCountTo', this.pCountTo.toString());

      this.visibilityStyle = 'hidden';

      window.scrollTo(0, 0);
    });
  }

  public getPaginationArray(questionCount, perPage, currentPage, maxPagesList = 4) {

    var pagination = new Array();
    var countPages = Math.ceil(questionCount / perPage);
    var firstPage = currentPage - (maxPagesList / 2);
    if (firstPage <= 1) {
      firstPage = 1;
    } else {
      if (countPages - firstPage < maxPagesList) {
        firstPage = countPages - maxPagesList + 1;
        if (firstPage <= 1) {
          firstPage = 1;
        }
      }
    }
    var lastPage = firstPage + maxPagesList - 1;
    if (lastPage > countPages)
      lastPage = countPages;

    for (var i = firstPage; i <= lastPage; ++i) {
      pagination.push(i);
    }

    return pagination;
  }

  onQuestionSave(question) {

    if (question.isFavourite == true) {

      question.isFavourite = false;
      this.questions[this.questions.indexOf(question)] = question;

      localStorage.setItem('questions', JSON.stringify(this.questions));
      this.applicationService.onRemoveFavourite(question.internal_id).subscribe(data => {
        if (data.resType) {
          alert(data.resErrors[0]);
        }
      });

    } else {

      question.isFavourite = true;

      this.questions[this.questions.indexOf(question)] = question;
      localStorage.setItem('questions', JSON.stringify(this.questions));

      this.applicationService.saveFavourite(question.internal_id, question.title, question.link, question.authorName, question.creationDate).subscribe(data => {
        if (data.resType) {
          alert(data.resErrors[0]);
        }
      });
    }
  }
}
