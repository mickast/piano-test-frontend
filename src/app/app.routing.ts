import {RouterModule, Routes} from '@angular/router';
import {SearchComponent} from './search/search.component';
import {RegisterComponent} from './register/register.component';
import {LoginComponent} from './login/login.component';
import {ModuleWithProviders} from '@angular/core';
import {HistoryComponent} from './history/history.component';
import {FavouritesComponent} from './favourites/favourites.component';
import { PageNotFoundComponent } from  './page-not-found/page-not-found.component';

const APP_ROUTES: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'search',
    component: SearchComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'history',
    component: HistoryComponent
  },
  {
    path: 'favourites',
    component: FavouritesComponent
  },
  {
    path: '',
    redirectTo: '/search',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

export const Routing: ModuleWithProviders = RouterModule.forRoot(APP_ROUTES);

