import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public registerError: string = 'Password and repeat password do not match';
  public loaderVisibilityStyle: string = 'hidden';
  public errorVisibilityStyle: string = 'none';

  constructor(private authService: AuthService, private router: Router) {
    if (this.authService.getAuthToken != '') {
      router.navigate(['/search']);
    }
  }

  onRegister(form: NgForm): void {
    this.loaderVisibilityStyle = 'visible';

    if (form.value.r_password != form.value.password) {
      this.errorVisibilityStyle = 'block';
    } else {
      this.authService.register(form.value.name, form.value.login, form.value.email, form.value.password, form.value.r_password).subscribe(
        data => {

          this.loaderVisibilityStyle = 'hidden';

          if (data.resType) {
            this.errorVisibilityStyle = 'block';
            this.registerError = data.resErrors[0];
          } else {
            this.authService.setAuthToken(data.resData.token);
            this.authService.setUserName(data.resData.userName);
            window.location.href = '/search';
          }
        }
      );
    }
  }

  ngOnInit() {
  }

}
