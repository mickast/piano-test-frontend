import {Component} from '@angular/core';
import {AuthService} from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  public title: string = 'Piano test application';
  public isNotAuthorized: boolean = false;
  public userName: string = '';

  constructor(private authService: AuthService) {
    this.userName = this.authService.userName;
    if (this.authService.getAuthToken == '') {
      this.isNotAuthorized = true;
    }
  }

  onLogout(): void {
    this.authService.logout();
    window.location.href = '/login';
  }
}
