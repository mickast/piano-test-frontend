import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginErrorMessage: string = 'Please check the spelling of your login and password.';

  public errorVisibilityStyle: string = 'hidden';

  public loaderVisibilityStyle: string = 'none';

  constructor(private authService: AuthService, private router: Router) {
    if (this.authService.getAuthToken != '') {
      router.navigate(['/search']);
    }
  }

  onLogin(form: NgForm): void {

    this.loaderVisibilityStyle = 'visible';

    this.authService.login(form.value.login, form.value.password).subscribe(data => {

      this.loaderVisibilityStyle = 'hidden';

      if (!data.resType) {

        this.authService.setUserName(data.resData.userName);
        this.authService.setAuthToken(data.resData.token);

        window.location.href = '/search';

      } else {
        this.errorVisibilityStyle = 'block';
      }
    });
  }

  ngOnInit() {
  }

}
