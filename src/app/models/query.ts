export class Query {
  tite: string;
  score: number;
  link: string;
  author_avatar: string;
  author_name: string;
  creation_date: number;
  isAnswered: boolean;
  tags: Array<string>;
  viewCount: number;
}
