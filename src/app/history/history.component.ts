import {Component, OnInit} from '@angular/core';
import { ApplicationService} from '../services/application.service';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {

  public queryHistory: Array<any> = [];

  public loaderVisibilityStyle: string = 'hidden';

  constructor(private applicationService: ApplicationService) { }

  ngOnInit() {

    this.loaderVisibilityStyle = 'visible';

    this.applicationService.getQueryHistory().subscribe(data => {
      this.loaderVisibilityStyle = 'hidden';
      this.queryHistory = data.resData.queries;
    });
  }
}
