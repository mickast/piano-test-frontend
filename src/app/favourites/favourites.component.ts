import {Component, OnInit, Renderer2, ViewChild} from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ApplicationService } from '../services/application.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-favourites',
  templateUrl: './favourites.component.html',
  styleUrls: ['./favourites.component.css']
})
export class FavouritesComponent implements OnInit {

  public favouriteQuestions: Array<any> = [];

  public loaderVisibilityStyle: string = 'hidden';

  constructor(public authService: AuthService, private applicationService: ApplicationService, private router: Router, private renderer: Renderer2) {
    if(this.authService.getAuthToken == ''){
      router.navigate(['/login']);
    }
  }

  onRemoveFavourite(question){

    let newArr = this.favouriteQuestions.filter(data => data != question);
    this.favouriteQuestions = newArr;
    this.applicationService.onRemoveFavourite(question.internal_id).subscribe(data => {
    });

  }

  ngOnInit() {
    this.loaderVisibilityStyle = 'visible';

    this.applicationService.getFavourite().subscribe(data => {

      this.loaderVisibilityStyle = 'hidden';
      this.favouriteQuestions = data.resData.questions;

    });
  }
}
